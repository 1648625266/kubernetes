# [3.5.0](https://gitlab.com/to-be-continuous/kubernetes/compare/3.4.3...3.5.0) (2023-03-14)


### Bug Fixes

* disable shellcheck rule ([5cf7f4e](https://gitlab.com/to-be-continuous/kubernetes/commit/5cf7f4e98e689b02ff7ae765e24caa924c141725))


### Features

* apply kustomize independently and allow setting ARGS ([d124593](https://gitlab.com/to-be-continuous/kubernetes/commit/d1245931d4dd96570412eecadd9418cd78405c1f))

## [3.4.3](https://gitlab.com/to-be-continuous/kubernetes/compare/3.4.2...3.4.3) (2023-02-01)


### Bug Fixes

* **KUBECONFIG:** select  to the given KUBE_CONTEXT if provided ([c31371c](https://gitlab.com/to-be-continuous/kubernetes/commit/c31371c7a61a770b3ecb34c17ef7f932f18fb033))

## [3.4.2](https://gitlab.com/to-be-continuous/kubernetes/compare/3.4.1...3.4.2) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([ef89e38](https://gitlab.com/to-be-continuous/kubernetes/commit/ef89e38bbe5e116effc96464c6deb351e90c63cb))

## [3.4.1](https://gitlab.com/to-be-continuous/kubernetes/compare/3.4.0...3.4.1) (2023-01-24)


### Bug Fixes

* **authent:** support text or file variable for Kubeconfig ([ce10ca3](https://gitlab.com/to-be-continuous/kubernetes/commit/ce10ca3efdf42fb1236a4eb286cd9135ccfb8796))

# [3.4.0](https://gitlab.com/to-be-continuous/kubernetes/compare/3.3.3...3.4.0) (2023-01-17)


### Features

* **score:** run score against cascading environments ([07dbbae](https://gitlab.com/to-be-continuous/kubernetes/commit/07dbbae7ca8491df422b1e78b11857dd5ba3bd9a))

## [3.3.3](https://gitlab.com/to-be-continuous/kubernetes/compare/3.3.2...3.3.3) (2023-01-09)


### Bug Fixes

* preserve priority even when mixing .yml and .yaml extensions ([078019d](https://gitlab.com/to-be-continuous/kubernetes/commit/078019da75e03111413dd986b5695b8b3bfeb5c2))

## [3.3.2](https://gitlab.com/to-be-continuous/kubernetes/compare/3.3.1...3.3.2) (2023-01-05)


### Bug Fixes

* use zegl/kube-score:latest by default ([2cb02bd](https://gitlab.com/to-be-continuous/kubernetes/commit/2cb02bd435419bc1624252a2dffb699f826a542b))

## [3.3.1](https://gitlab.com/to-be-continuous/kubernetes/compare/3.3.0...3.3.1) (2022-12-17)


### Bug Fixes

* hanging awk script ([c658c4a](https://gitlab.com/to-be-continuous/kubernetes/commit/c658c4aa8d2664f2f78952ad53abb3a415d084a3))

# [3.3.0](https://gitlab.com/to-be-continuous/kubernetes/compare/3.2.0...3.3.0) (2022-12-15)


### Features

* support dynamic env url ([0e97232](https://gitlab.com/to-be-continuous/kubernetes/commit/0e97232acd6700b28e33de2caf92c12a7d7aa473))

# [3.2.0](https://gitlab.com/to-be-continuous/kubernetes/compare/3.1.1...3.2.0) (2022-12-13)


### Bug Fixes

* **score:** declare package-test stage ([bc5c7fa](https://gitlab.com/to-be-continuous/kubernetes/commit/bc5c7fa690bbf9c855e70d7dca75278a2e2e7211))
* **score:** move kubescore to 'package-test' stage (if package variables used in manifests) ([a6e07eb](https://gitlab.com/to-be-continuous/kubernetes/commit/a6e07eb9ebb5ef66e969e7f640534fcb4bef41da))


### Features

* **vault:** configurable Vault Secrets Provider image ([1280d8a](https://gitlab.com/to-be-continuous/kubernetes/commit/1280d8a95a7de7b02efbcd15ab4939a2743ebcef))

## [3.1.1](https://gitlab.com/to-be-continuous/kubernetes/compare/3.1.0...3.1.1) (2022-11-07)


### Bug Fixes

* launch score on generated manifest file ([17b5d05](https://gitlab.com/to-be-continuous/kubernetes/commit/17b5d051688a3714b360df3ab34ec6f028209fb6))
* use namespace from template vars ([1e326c1](https://gitlab.com/to-be-continuous/kubernetes/commit/1e326c11b7455561e5fd419685505a2ca03394eb))

# [3.1.0](https://gitlab.com/to-be-continuous/kubernetes/compare/3.0.0...3.1.0) (2022-10-12)


### Features

* enable kustomize support ([3ad8b84](https://gitlab.com/to-be-continuous/kubernetes/commit/3ad8b84a4e626273a6c03af2dc98e834ac8cc234))

# [3.0.0](https://gitlab.com/to-be-continuous/kubernetes/compare/2.3.2...3.0.0) (2022-08-05)


### Features

* adaptive pipeline ([863412f](https://gitlab.com/to-be-continuous/kubernetes/commit/863412f5ea8e2812ded6e7578045ca382fd94657))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

## [2.3.2](https://gitlab.com/to-be-continuous/kubernetes/compare/2.3.1...2.3.2) (2022-07-01)


### Bug Fixes

* set ENV_VAR_SUFFIX on cleanup ([78f1025](https://gitlab.com/to-be-continuous/kubernetes/commit/78f10257b5bd873f1fa606fdb6c61ed7df460f6b))

## [2.3.1](https://gitlab.com/to-be-continuous/kubernetes/compare/2.3.0...2.3.1) (2022-06-30)


### Bug Fixes

* enforce AUTODEPLOY_TO_PROD as boolean variable ([154ca7a](https://gitlab.com/to-be-continuous/kubernetes/commit/154ca7aca56b19dbf8b0c926545f6ff271f9de67))

# [2.3.0](https://gitlab.com/to-be-continuous/kubernetes/compare/2.2.0...2.3.0) (2022-06-30)


### Features

* enforce AUTODEPLOY_TO_PROD and PUBLISH_ON_PROD as boolean variables ([5d3c5a5](https://gitlab.com/to-be-continuous/kubernetes/commit/5d3c5a5ced96306d20d1ab8cbc070c5da4f16798))

# [2.2.0](https://gitlab.com/to-be-continuous/kubernetes/compare/2.1.0...2.2.0) (2022-05-01)


### Features

* configurable tracking image ([75e5cc5](https://gitlab.com/to-be-continuous/kubernetes/commit/75e5cc5b516fa5023fea26241403fa03e37d2f18))

# [2.1.0](https://gitlab.com/to-be-continuous/kubernetes/compare/2.0.7...2.1.0) (2022-04-26)


### Features

* keyword to prevent variables substitution ([24fa867](https://gitlab.com/to-be-continuous/kubernetes/commit/24fa8674c4e9a5e15a08aede8f731957bc4535eb))

## [2.0.7](https://gitlab.com/to-be-continuous/kubernetes/compare/2.0.6...2.0.7) (2022-04-22)


### Bug Fixes

* Export 'awkenvsubst' function ([fe768a1](https://gitlab.com/to-be-continuous/kubernetes/commit/fe768a1dfd43b2673247cd5a331065b1d251671b)), closes [#18](https://gitlab.com/to-be-continuous/kubernetes/issues/18)

## [2.0.6](https://gitlab.com/to-be-continuous/kubernetes/compare/2.0.5...2.0.6) (2022-02-24)


### Bug Fixes

* **vault:** revert Vault JWT authentication not working ([4ee0725](https://gitlab.com/to-be-continuous/kubernetes/commit/4ee0725c38c572842efcece1ba37bb5747ac0533))

## [2.0.5](https://gitlab.com/to-be-continuous/kubernetes/compare/2.0.4...2.0.5) (2022-01-10)


### Bug Fixes

* non-blocking warning in case failed decoding [@url](https://gitlab.com/url)@ variable ([162cd38](https://gitlab.com/to-be-continuous/kubernetes/commit/162cd380229369f8cf7aa865e8db8d807b5e3b54))

## [2.0.4](https://gitlab.com/to-be-continuous/kubernetes/compare/2.0.3...2.0.4) (2021-12-12)


### Bug Fixes

* permission problem on chmod ([ce2012e](https://gitlab.com/to-be-continuous/kubernetes/commit/ce2012e497a339ead6ab356fb944fc49a7b5e399))

## [2.0.3](https://gitlab.com/to-be-continuous/kubernetes/compare/2.0.2...2.0.3) (2021-12-03)


### Bug Fixes

* execute hook scripts with shebang shell ([9eac25d](https://gitlab.com/to-be-continuous/kubernetes/commit/9eac25d9e41d46b7522dd7ea25eee5ee61c8f667))

## [2.0.2](https://gitlab.com/to-be-continuous/kubernetes/compare/2.0.1...2.0.2) (2021-10-07)


### Bug Fixes

* use master or main for production env ([0822400](https://gitlab.com/to-be-continuous/kubernetes/commit/0822400ecd6c4965c3ba23d06b88c925497d7017))

## [2.0.1](https://gitlab.com/to-be-continuous/kubernetes/compare/2.0.0...2.0.1) (2021-09-16)


### Bug Fixes

* vault variant ([a484505](https://gitlab.com/to-be-continuous/kubernetes/commit/a484505184ab50ea958af488db06cb498c267dc1))

## [2.0.0](https://gitlab.com/to-be-continuous/kubernetes/compare/1.3.1...2.0.0) (2021-09-03)

### Features

* Change boolean variable behaviour ([3bd6a03](https://gitlab.com/to-be-continuous/kubernetes/commit/3bd6a03f63f3083d6982d37f3bada6cc9cd8c08c))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.3.1](https://gitlab.com/to-be-continuous/kubernetes/compare/1.3.0...1.3.1) (2021-07-08)

### Bug Fixes

* conflict between vault and scoped vars ([ed07480](https://gitlab.com/to-be-continuous/kubernetes/commit/ed074803fa0998586bd27d0c1c76eed35c069123))

## [1.3.0](https://gitlab.com/to-be-continuous/kubernetes/compare/1.2.0...1.3.0) (2021-06-19)

### Features

* support multi-lines environment variables substitution ([bc2f8d5](https://gitlab.com/to-be-continuous/kubernetes/commit/bc2f8d58ccdef31bd66afb845f8877007e91f026))

## [1.2.0](https://gitlab.com/to-be-continuous/kubernetes/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([0042198](https://gitlab.com/to-be-continuous/kubernetes/commit/0042198c0f6e5c14877f5693b986dc778d9b5fef))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/kubernetes/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([f2c8894](https://gitlab.com/Orange-OpenSource/tbc/kubernetes/commit/f2c8894eb1f8d73fa7d37a93553a3f3b50f670f9))

## 1.0.0 (2021-05-06)

### Features

* initial release ([885caed](https://gitlab.com/Orange-OpenSource/tbc/kubernetes/commit/885caed8b3063252d72abbdb568f40a59e99585e))
